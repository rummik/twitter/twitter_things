var Twit = require('twit');
require('colors');

function trim(a) { return a.trim(); }
function logerr(err) { err && console.error('error:', err); }

function ThingBot(config) {
	this.twit = new Twit(config.twitter);

	this.name = '';
	this.kind = '';

	this.words = config.words || [
		'sock',
		'blog',
		'barrel',
		'tweet',
		'intern',
		'apple',
		'song',
		'computer'
	];

	this.rate = {};

	Object.defineProperty(this, 'handle', { get: (function() {
		return '@' + this.name;
	}).bind(this)});


	this.getInfo(function() {
		setInterval(this.getInfo.bind(this), 60 * 60 * 1000);

		(function randomStatus(yes) {
			yes && this.statusUpdate();
			setTimeout(randomStatus.bind(this, 1), (Math.random() * 30 + 30) * 60 * 1000);
		}).apply(this);
	});

	return;

	this.twit.stream('user').on('follow', (function(event) {
		if (event.source.screen_name == this.name) {
			return;
		}

		this.log('following', ('@' + event.source.screen_name).blue);
		this.twit.post('friendships/create', { user_id: event.source.id_str }, logerr);
	}).bind(this));

	this.twit.stream('user').on('tweet', (function(update) {
		if (!this.name || update.retweeted_status || update.direct_message ||
		    update.user.screen_name == this.name) {
			return;
		}

		var sender = update.user.screen_name.toLowerCase();
		var screen_name = ('@' + update.user.screen_name).blue;
		var words = (update.text.match(/(^|\s)(?!@)\S+(?=\s|$)/g) || []).map(trim);
		var mentions = (update.text.match(/(^|\s)@([a-z0-9_]+)/gi) || [])
		                .map(trim)
		                .filter((function(a) {
		                	return a != this.handle;
		                }).bind(this));

		var message = '';
		var status = {
			in_reply_to_status_id: update.id_str,
			status: mentions.concat('@' + sender).join(' ') + ' ',
		};

		if (words.join(' ').toLowerCase().indexOf(this.kind) !== -1) {
			this.log('debug', words.join(' '), JSON.stringify(update.entities));
		}

		var entities = update.entities;
		if (!(entities.urls.length || entities.media) &&
		    words.join(' ').toLowerCase().indexOf(this.kind) !== -1) {
			this.log('favoriting', screen_name, update.text);
			this.twit.post('favorites/create', { id: update.id_str }, logerr);
		}

		if ((sender.indexOf('horse_') >= 0 || sender.indexOf('_ebooks') >= 0 ||
		     config.bots.indexOf(sender) >= 0) && Math.random() < 0.2) {
			return;
		}

		if (this.rate[sender] > 3) {
			this.log('I think'.red, screen_name, 'is very talkative'.red);
		}

		if (this.rate[sender] > 3 && Math.random() < this.rate[sender] * this.rate[sender] / 1000) {
			return;
		}

		if (sender.indexOf('_things') >= 0) {
			if (Math.random() <= 0.02) {
				this.log('arguing with', screen_name);
				message = this.kind.toUpperCase() + ' ' + words.slice(1).join(' ');
			} else {
				this.log('not arguing with', screen_name);
			}
		} else if (/*Math.random() <= 0.003 || */update.text.indexOf(this.handle) >= 0) {
			this.log('replying to', screen_name, update.text);
			message = this.thingString();
		}

		if (message) {
			if (this.rate[sender]) {
				this.rate[sender]++;
			} else {
				this.rate[sender] = 0;
			}

			var timeout = Math.random() * 55 * 1000 + 5 * 1000;
			timeout += this.rate[sender] * 10 * 1000;
			this.log(status.status += message, 'in', Math.floor(timeout / 1000), 'seconds');
			setTimeout(this.twit.post.bind(this.twit, 'statuses/update', status, logerr), timeout);
		}
	}).bind(this));

	(function limitTick() {
		Object.keys(this.rate).forEach((function(handle) {
			if (this.rate[handle] > 1) {
				this.rate[handle]--;
			} else {
				delete this.rate[handle];
			}
		}).bind(this));

		setTimeout(limitTick.bind(this), 5 * 60 * 1000);
	}).apply(this);
}

ThingBot.prototype.log = function log() {
	var args = Array.prototype.slice.apply(arguments);
	console.log.apply(console, [this.handle.green + ':'].concat(args));
};

ThingBot.prototype.thingString = function thingString() {
	return this.kind + ' ' + this.words[Math.floor(Math.random() * this.words.length)];
};

ThingBot.prototype.getInfo = function updateInfo(callback) {
	this.twit.get('account/settings', (function(err, settings) {
		logerr(err);

		this.name = settings.screen_name;
		this.kind = this.name.replace(/_things$/, '').replace(/_/g, ' ');

		this.log('my name is', this.name.cyan);
		this.log('I am a', this.kind.cyan, 'robot');

		if (typeof callback == 'function') {
			callback.apply(this);
		}
	}).bind(this));
};

ThingBot.prototype.statusUpdate = function statusUpdate() {
	var statusThing = this.thingString();
	this.log('tweeting a', statusThing);
	this.twit.post('statuses/update', { status: statusThing }, logerr);
};

module.exports = ThingBot;
